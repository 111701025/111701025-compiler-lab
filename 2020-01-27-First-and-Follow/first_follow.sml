use "grammar.sml";
use "example.sml";
(*
declaration and intialisation of first, follow and nullable llist
*)


(*
nullable contains the symbols which can be nullable
*)

val nullable : Atom.atom list ref = ref nil;

(* first and follow should store the symbols and there corresponding first elements and follow elements
hence we use map of sets
 *)

val first : (AtomSet.set ref) AtomMap.map ref = ref AtomMap.empty;
val follow : (AtomSet.set ref) AtomMap.map ref = ref AtomMap.empty;

(*
initialisation
*)

fun initialise given1 = let 
                        fun insert_symbol (x::xs) = (given1 := AtomMap.insert (!given1,x, ref AtomSet.empty); insert_symbol xs)
                        | insert_symbol _ = ()
                        in 
                        insert_symbol(AtomSet.listItems (#symbols grammar))
                        end;


initialise first;
initialise follow;

(*
finding all nullable symbols
*)

(*
we need a terminating condition to stop checking for any more symbols. we do this by fixed point
*)

fun is_present lst x = List.exists (fn y => (Atom.compare(x,y) = EQUAL)) lst;

val key = ref false;

(*
check whether given token is nullable or not
*)

fun check_nullable_token x = if Atom.compare (x, Atom.atom "EPS") = EQUAL then true
                                else if AtomSet.member (#tokens grammar, x) then false
                                else is_present (!nullable) x; 

(*
check given production is nullable.
if all symbols and tokens on the right are nullable then the production is nullable
*)

fun check_nullable_production (x::xs) = if not (check_nullable_token x) then false else check_nullable_production xs
        | check_nullable_production _ = true;

(*
check given rules for a symbol are nullable.
we need to check all the rules in which the needed symbol is on left of the rule
*)

fun check_nullable_rule (x::xs) = if check_nullable_production x then true else check_nullable_rule xs
        | check_nullable_rule _ = false;

(*
check whether the symbols is nullable.
check the rules of that symbol
*)

fun check_nullable_symbol x = let val rl = RHSSet.listItems (AtomMap.lookup (#rules grammar, x)) in
                                    check_nullable_rule rl
                                end;

(*
finding all the nullable symbols
every time we add a new symbol to the nullable list we need check all the symbols again to find whether they are nullable
*)

fun check_nullable_symbols (x::xs) = (if is_present (!nullable) x then ()
                    else if check_nullable_symbol x then (nullable := x :: !nullable; key := true)
                    else ();check_nullable_symbols xs)
|   check_nullable_symbols _ = ();

(*
finding and inserting the symbols in the list
*)

fun find_nullable () = (key := false; check_nullable_symbols (AtomSet.listItems (#symbols grammar));
                        if (!key) then find_nullable () else ());

(*
nullable completed
*)

(*
finding the first of all symbols.
we need to find the produciton rules and check all the terminals which can be generated using the given symbol.
for that we need to find the first of the first symbol in the production and if it is nullable then continue till the end or if any symbol is not nullable
*)

(*
finding if for the given symbol whether the first includes the given token.
or if it is a symbol check the first of that symbol
*)

fun add_to_first a b = let val first_b = if AtomSet.member (#tokens grammar, b) then AtomSet.add (AtomSet.empty, b)
                                        else if Atom.same (b, Atom.atom "EPS") then AtomSet.empty
                                        else !(AtomMap.lookup (!first, b))
                
                           val first_a = !(AtomMap.lookup (!first, a)) 
                        in
                                if AtomSet.isSubset (first_b, first_a) then () 
                                else (key := true; AtomMap.lookup (!first, a) := AtomSet.union(first_a, first_b))
                        end;

(*
finding the first of a symbol for a particular production
*)
fun find_first_production a (x::xs) = (add_to_first a x;if is_present (!nullable) x then find_first_production a xs else () )
    | find_first_production _ _ = ();

(*
find the first of a symbol from all of its productions
*)

fun find_first_rule a (x::xs) = (find_first_production a x;find_first_rule a xs)
    | find_first_rule _ _     = ();

(*
getting all the production rules for a particular symbol
*)

fun find_first_symbol x = let val rl = RHSSet.listItems (AtomMap.lookup(#rules grammar, x) ) in 
                            find_first_rule x rl
                          end;


(*
finding first for all symbols
*)

fun find_first_symbols (x::xs) = (find_first_symbol x;find_first_symbols xs)
    |find_first_symbols _   = ();

(*
finding and inserting the first of each symbol and inserting them into a AtomMap
*)

fun find_first () = (key:= false; find_first_symbols (AtomSet.listItems (#symbols grammar) );
            if (!key) then find_first () else () );



(*
follow is similar to first but follow has 2 rules.
*) 

fun add_follow_symbol y x (xs::xss) = (let val fst_xs = if AtomSet.member (#tokens grammar, xs) then AtomSet.add (AtomSet.empty, xs)
                                                        else !(AtomMap.lookup (!first, xs))
                                                val foll_x = !(AtomMap.lookup (!follow, x)) in
                                            if AtomSet.isSubset (fst_xs, foll_x) then ()
                                            else (key := true; AtomMap.lookup (!follow, x) := AtomSet.union (fst_xs, foll_x))
                                        end;
                                        if is_present (!nullable) xs then add_follow_symbol y x xss else ())
|   add_follow_symbol y x _ = let val foll_y = !(AtomMap.lookup (!follow, y))
                                        val foll_x = !(AtomMap.lookup (!follow, x)) in
                                    if AtomSet.isSubset (foll_y, foll_x) then ()
                                    else (key := true; AtomMap.lookup (!follow, x) := AtomSet.union (foll_x, foll_y))
                                end;



fun find_follow_prod y (x::xs) = (if AtomSet.member (#symbols grammar, x) then add_follow_symbol y x xs else ();
                                  find_follow_prod y xs)
|   find_follow_prod _ _ = ();



fun find_follow_rule x (r::rules) = (find_follow_prod x r; find_follow_rule x rules)
|   find_follow_rule _ _ = ();


fun find_follow_symbol x = let val xrules = RHSSet.listItems (AtomMap.lookup(#rules grammar, x)) in
                find_follow_rule x xrules
               end;


fun find_follow_symbols (x::xs) = (find_follow_symbol x; find_follow_symbols xs)
|   find_follow_symbols _   = ();


fun find_follow () = (key:= false; find_follow_symbols (AtomSet.listItems (#symbols grammar) ) ;
            if(!key) then find_follow () else () );

find_nullable ();
find_first ();
find_follow ();

fun  print_atom_list [] = "\n"
    | print_atom_list (x1::[]) =(Atom.toString x1)^" \n"
    | print_atom_list (x1::xs) = (Atom.toString x1)^", " ^ (print_atom_list xs)

fun print_set_list ((a,b)::xs) = ((Atom.toString a)^":-\t"^(print_atom_list (AtomSet.listItems (!b))))^(print_set_list xs)
    |print_set_list _   = "\n";

(*printing nullable *)

print("The list of nullable symbols : \t"^print_atom_list (!nullable));
(*printing first sets *)

print("The list of first sets for the each symbol in the grammar :\n"^(print_set_list (AtomMap.listItemsi (!first) )));


(*printing follow sets *)
print("The list of follow sets for the each symbol in the grammar :\n"^(print_set_list (AtomMap.listItemsi (!follow) )));

