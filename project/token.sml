datatype Keywords = ARRAY
		| IF
		| THEN
		| ELSE
		| WHILE
		| FOR
		| TO
		| DO
		| LET
		| IN
		| END
		| OF
		| BREAK
		| NIL
		| FUNCTION
		| VAR
		| TYPE
		| IMPORT
		| PRIMITIVE

datatype Objkey = CLASS
		| EXTENDS
		| METHOD
		| New

datatype Symbols = DOT
		| COMMA
		| COLON
		| SEMICOLON
		| LPAREN
		| RPAREN
		| LSQ
		| RSQ
		| LFB
		| RFB
		| PLUS
		| MINUS
		| MUL
		| DIVIDE
		| EQUAL
		| LTGT
		| LTHAN
		| GTHAN
		| LEQUAL
		| GEQUAL
		| AND
		| OR
		| CEQUAL

datatype Whitespace = SPACE
		| Tab

datatype Token = NEWLINE
		| Space of (int*Whitespace) 
		| End
		| Key of Keywords
		| Obj of Objkey
		| Sym of Symbols
		| COMMENTS of string
		| QUOTE of string
		| TEXT of string
		| CONST of int
		
