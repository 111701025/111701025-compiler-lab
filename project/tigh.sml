structure tigh =
struct

val interactive = Tiglex.makeLexer (fn _ => TextIO.inputN (TextIO.stdIn,1))

fun lexfile file = let val strm = TextIO.openIn file
		   in Tiglex.makeLexer (fn n => TextIO.inputN(strm,n))
		   end
fun spaces(1,Tab)="\t" |	
spaces (n,Tab)  = "\t"^spaces(n-1,Tab) | 
spaces (1,SPACE)=" "|
spaces (n,SPACE)=" "^spaces(n-1,SPACE);

fun fromtoken token = case token of
		(COMMENTS x)	=> x
		|NEWLINE	=> ""
		|End		=> ""
		|(CONST y)	=> Int.toString(y)
		|(TEXT x)	=> x
		|(QUOTE x)	=> x
		|(Space(n,x))	=> spaces(n,x)
		|(Key ARRAY)	=> "array"
		|(Key IF)	=> "if"
		|(Key THEN)	=> "then"
		|(Key ELSE)	=> "else"
		|(Key WHILE)	=> "while"
		|(Key FOR)	=> "for"
		|(Key TO)	=> "to"
		|(Key DO)	=> "do"
		|(Key IN)	=> "in"
		|(Key END)	=> "end"
		|(Key OF)	=> "of"
		|(Key BREAK)	=> "break"
		|(Key NIL)	=> "nil"
		|(Key FUNCTION)	=> "function"
		|(Key VAR)	=> "var"
		|(Key TYPE)	=> "type"
		|(Key IMPORT)	=> "import"
		|(Key PRIMITIVE)=> "primitive"
		|(Key LET)	=> "let"
		|(Obj CLASS)	=> "class"
		|(Obj EXTENDS)	=> "extends"
		|(Obj New)	=> "new"
		|(Obj METHODS) 	=> "method"
		|(Sym DOT)	=> "."
		|(Sym COMMA)	=> ","
		|(Sym COLON)	=> ":"
		|(Sym SEMICOLON)=> ";"
		|(Sym LPAREN)	=> "("
		|(Sym RPAREN)	=> ")"
		|(Sym LSQ)	=> "["
		|(Sym RSQ)	=> "]"
		|(Sym LFB)	=> "{"
		|(Sym RFB)	=> "}"
		|(Sym PLUS)	=> "+"
		|(Sym MINUS)	=> "-"
		|(Sym MUL)	=> "*"
		|(Sym DIVIDE)	=> "/"
		|(Sym EQUAL)	=> "="
		|(Sym LTGT)	=> "<>"
		|(Sym LTHAN)	=> "<"
		|(Sym GTHAN)	=> ">"
		|(Sym LEQUAL)	=> "<="
		|(Sym GEQUAL)	=> ">="
		|(Sym AND)	=> "&"
		|(Sym OR)	=> "|"
		|(Sym CEQUAL)	=> ":="

fun colour token = case token of
	(NEWLINE)	=> print("\n")
	|(Space(n,x))	=> print(fromtoken(token))
	|(Key _)	=> print("\027[94m"^fromtoken(token))
	|(Obj _)	=> print("\027[32m"^fromtoken(token))
	|(CONST _)	=> print("\027[34m"^fromtoken(token))
	|(QUOTE _)	=> print("\027[37m \""^fromtoken(token)^"\"")
	|(Sym _)	=> print("\027[35m"^fromtoken(token))
	|(COMMENTS _)	=> print("\027[31m"^fromtoken(token))
	|(End)		=> print("\027[0m")
	|(TEXT _)	=> print("\027[96m"^fromtoken(token))

fun runWithLexer lexer = let fun loop() = case lexer () of
					        (token)   => loop (colour token)
			 in loop()
			 end

val _ =  ( case CommandLine.arguments() of
	       [] => runWithLexer interactive
	    |  xs => (List.map (runWithLexer o lexfile) xs; ())
	 )

end
